#In

Include or Insert template files. A light alternative to snippets and embeds.

![In](https://www.causingeffect.com/images/software/in.jpg)

##Usage Examples

###Insert

Inserts a template as an early-parsed global variable or snippet.

	{in:sert:blog/.blog_entries}

If you are using MSM, you can specify the site short name like this:

	{in:sert:site_short_name:blog/.blog_entries}

See the [Insert](http://www.causingeffect.com/software/expressionengine/in/user-guide/usage#insert) documentation for more info.

###Include

Includes a template file. It's basically an embed that is parsed as an exp tag, instead of much later in the parse order.

	{exp:in:clude:includes/.header default_description="{home_meta_description}" default_keywords="{home_meta_keywords}"}

If you are using MSM, you can specify the site short name like this:

	{exp:in:clude:site_short_name:includes/.header default_description="{home_meta_description}" default_keywords="{home_meta_keywords}"}

See the [Include](http://www.causingeffect.com/software/expressionengine/in/user-guide/usage#include) documentation for more info.


##Links

* [Requirements](http://www.causingeffect.com/software/expressionengine/in/requirements)
* [User Guide](http://www.causingeffect.com/software/expressionengine/in/user-guide)
* [Change Log](http://www.causingeffect.com/software/expressionengine/in/change-log)
* [Support](http://www.causingeffect.com/software/expressionengine/in/support)
* [Download Locations](http://www.causingeffect.com/software/expressionengine/in/download)



##MIT License

Copyright © 2014 Causing Effect, Aaron Waldon

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
